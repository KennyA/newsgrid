import { defineStore } from 'pinia'

import { Nav } from '@/models/nav'
import { Item } from '@/models/item'

export const useNewsStore = defineStore('news', {
  state: () => ({
    nav: {} as Nav,
    items: [] as Item[],
  }),
  actions: {
    showMore():void {
      if (this.nav?.current) {
        this.nav.current++
      }
    },
    async fetchNews(): Promise<void> {
      try {
        const url = `http://flems.github.io/test/api/news/${this.nav?.current ? this.nav.current : 1}/`
        const response = await fetch(url)
        const itemsRes = await response.json()
        itemsRes.items.forEach((item: any) => {
          this.items.push(new Item(item))
        })
        this.nav = new Nav(itemsRes.nav)
      } catch (error) {
        console.error(error)
      }
    }
  }
})
