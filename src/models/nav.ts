export class Nav {
  current!: number
  total!: number
  
  constructor(res:any) {
    this.current = res.current ? res.current : 0
    this.total = res.total ? res.total : 0
  }
}