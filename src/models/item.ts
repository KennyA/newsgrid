export class Item {
  date!: number
  image!: string
  link!: string
  name!: string
  previewText!: string
  type!: {value:string, xmlId:number}

  constructor(res:any) {
    this.date = res.date ? res.date : 0
    this.image = res.image ? res.image : ''
    this.link = res.link ? res.link : ''
    this.name = res.name ? res.name : ''
    this.previewText = res.previewText ? res.previewText : ''
    this.type = res.type?.value && res.type.xmlId ? res.type : {value: '', xmlId: ''}
  }
}